class PostsController < ApplicationController
  before_action :ensure_logged_in

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    if @post.save
      render :show
    else
      flash.now[:error] = @post.errors.full_messages
      render :new
    end
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = current_user.authored_posts.find(params[:id])
    if @post.update(post_params)
      render :show
    else
      flash.now[:errors] = @post.errors.full_messages
      render :edit
    end
  end

  def show
    @post = Post.find(params[:id])
    @all_comments = Comment.all
    @parent_comments = @post.comments.where(parent_comment_id: nil)
    @third_guy = @all_comments - @parent_comments

  end


  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to sub_url(@post.sub_forum)
  end

  private

  def post_params
    params.require(:post).permit(:title, :url, :content, sub_ids: [])
  end
end
