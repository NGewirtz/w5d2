Rails.application.routes.draw do
  get 'comments/new'

  resources :subs
  resources :posts do
    resources :comments, only: :new
  end

  resources :users
  resource :session


  resources :comments, only: [:create, :show]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
